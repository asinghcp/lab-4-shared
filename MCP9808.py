'''
@file MCP9808.py

@brief file Driver for MCP9808 Temperature Sensor

@details This file contains a driver for the MCP9808 temperature sensor for
         use with the Nucleo. The user can communicate with the MCP9808 using
         its I2C interface. The driver contains an initializer, a check method 
         that verifies the sensor is correctly attatched, a celsius method
         that returns the temperature in Celsius, and a fahrenheit method that
         returns the temperature in Fahrenheit.
         \n\n Source Code: https://bitbucket.org/asinghcp/lab-4-shared/src/master/MCP9808.py
         
@author Anil Singh & Benjamin Presley

@date February 11, 2021

@copyright Available for use with consent of authors.
'''

from pyb import I2C
import utime

class MCP9808:
       
    def __init__(self, address):
        '''
        @brief initializes the i2c MCP9808 board
        @param address Device address for connected MCP9808 (unique to each board)
        '''
        ## Create I2C on Bus 1 and initiate as MASTER
        self.i2c = I2C(1, I2C.MASTER)
        self.addr = address  #define the address (this is different for each board)
        
    def check(self): #check method
        '''
        @brief function quickly confirms status of serial bus.
        @details This function promptly checks on the status of the serial
                 I2C bus and if its ID mathces the manufactur ID of the 
                 MCP9808.
        @returns a true or false boolean
        '''
        ## Buffer Array to write from Manufacturer ID Register
        buffer_check = bytearray(2)
        
        #Write into buffer check from manufacturer ID register
        self.i2c.mem_read(buffer_check, addr = self.addr, memaddr = 6)
        
        #Check if Published Manufacturer ID matches received ID
        if buffer_check[1] == 0x0054:
            
            return True
        else:
            return False
            
    def fahrenheit(self): #Fahrenheit method
        '''
        @brief function quickly fetches i2c data and returns Fahrenheit temp.
        @detail The data is fetched from the i2c device, and needs to go through
                proper conversion because it is not a temperature yet,
                this function just quickly reads the necessary bits needed for
                other function to convert it into degrees of Fahrenheit!
        @return temperature in degrees Fahrenheit
        '''
        buffer_array = bytearray(2)
        self.i2c.mem_read(buffer_array, addr = self.addr, memaddr = 5)
        buffer_array[0] = buffer_array[0] & 0x1F
        
        # Check if data is below 0*C
        if buffer_array[0] & 0x10 == 0x10:
            
            # If data is below 0*C, clear sign and "resign" in integer calc
            buffer_array[0] = buffer_array[0] & 0x0F
            temperature = (buffer_array[0]*16+buffer_array[1]/16)-256
            
        # Runs if data indicates above 0*C
        else:
            temperature = buffer_array[0]*16+buffer_array[1]/16
            temperatureF = temperature*(9/5)+32
            
        return temperatureF
    
    def celsius(self): #Celsius method
        '''
        @brief function quickly fethces i2c data and returns Celcius temp.
        @detail The data is fetched from the i2c device, and needs to go through
                proper conversion because it is not a temperature yet,
                this function just quickly reads the necessary bits needed for
                other function to convert it into degrees of Celcius!
        @return temperature in degrees Celcius
        '''
        ## Buffer Array to write Temperature Data Into
        buffer_array = bytearray(2)
        
        #Write Temperature Data to buffer array from register on MCP9808
        self.i2c.mem_read(buffer_array, addr = self.addr, memaddr = 5)
        
        #Clear Flags
        buffer_array[0] = buffer_array[0] & 0x1F
        
        # Check if data is below 0*C
        if buffer_array[0] & 0x10 == 0x10:
            
            # If data is below 0*C, clear sign and reassign integer calc
            buffer_array[0] = buffer_array[0] & 0x0F
            temperature = (buffer_array[0]*16+buffer_array[1]/16)-256 #Convert to decimal
            
        # Runs if data indicates above 0*C
        else: 
            temperature = buffer_array[0]*16+buffer_array[1]/16 #Convert to decimal
            
        return temperature    
    
if __name__ == '__main__': #test section
    #This function returns the temperature in degrees celsius every second
    mcp9808 = MCP9808(24) #make sure to update with your own i2c address!
    #Check that Manufacturer ID matches published.
    mcp9808.check()
    while True:
        print(mcp9808.celsius())
        utime.sleep(1)